package com.iisysgroup.androidlite.vas.activity.energy.Eko

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import com.afollestad.materialdialogs.MaterialDialog
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.iisysgroup.androidlite.App
import com.iisysgroup.androidlite.R
import com.iisysgroup.androidlite.TermMagmActivity
import com.iisysgroup.androidlite.cardpaymentprocessors.PurchaseProcessor
import com.iisysgroup.androidlite.cardpaymentprocessors.VasPurchaseProcessor
import com.iisysgroup.androidlite.login.Helper
import com.iisysgroup.androidlite.login.securestorage.SecureStorage
import com.iisysgroup.androidlite.models.BaseReceiptDetails
import com.iisysgroup.androidlite.payments_menu.BasePaymentActivity
import com.iisysgroup.androidlite.utils.PinAlertUtils
import com.iisysgroup.androidlite.utils.SharedPreferenceUtils
import com.iisysgroup.androidlite.vas.services.EkoService
import com.iisysgroup.payvice.securestorage.SecureStorageUtils
import com.iisysgroup.poslib.deviceinterface.DeviceState
import com.iisysgroup.poslib.utils.AccountType
import kotlinx.android.synthetic.main.activity_eko_prepaid.*
import kotlinx.android.synthetic.main.content_prepaid.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.okButton
import org.jetbrains.anko.toast
import java.net.ConnectException
import java.net.SocketTimeoutException

@Suppress("IMPLICIT_CAST_TO_ANY")
class EkoPrepaid : AppCompatActivity(), PinAlertUtils.PinEnteredListener {
    override fun onPinEntered(pin: String?) {
         val ePassword = SecureStorage.retrieve(Helper.STORED_PASSWORD,"")
         val encryptedPin = SecureStorageUtils.hashIt(pin!!, ePassword)
        this.pin = pin!!
        if (isCard){
            payWithCard(meterNumber, phoneNumber, amount, pin!!)
        } else {
            payWithWallet(meterNumber, phoneNumber, amount, false, pin!!)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId){
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return false
    }
    private lateinit var lookupResponse : EkoModel.EkoLookUpSuccessResponse

    private lateinit var meterNumber : String
    private lateinit var amount : String
    private lateinit var phoneNumber : String
    private lateinit var pin : String


    private val wallet_username by lazy {
        SharedPreferenceUtils.getPayviceUsername(this)
    }

    private val wallet_id by lazy {
        SharedPreferenceUtils.getPayviceWalletId(this)
    }

    private val wallet_password by lazy {
        SecureStorage.retrieve(Helper.PLAIN_PASSWORD, "")
    }

    private val mProgressDialog by lazy {
        indeterminateProgressDialog("Processing")
    }


    private var isCard : Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_eko_prepaid)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        submit.setOnClickListener { showPhoneNumberInput() }
    }

    private fun showPhoneNumberInput() {
        val meterNumber = customerNumber.text.toString()

        if (meterNumber.length < 8){
            this.customerNumber.error = "Enter valid meter number"
            return
        }

        MaterialDialog.Builder(this@EkoPrepaid).title("Phone number input").content("Please enter your phone number").inputType(InputType.TYPE_CLASS_PHONE).input("Phone number", "") { _, input -> handleEkoPayments(input.toString(), meterNumber) }.show()

    }

    private fun handleEkoPayments(phoneNumber : String, meterNumber: String) {
        if (phoneNumber.length != 11){
            toast("Enter valid phone number")
            return
        }
        mProgressDialog.show()

        async {
            try {
                val ekoDetails = EkoModel.EkoLookupDetails(meter = meterNumber)
                val response = EkoService.create().ekoLookup(ekoDetails).await()

                val jsonResponse = Gson().toJsonTree(response)
                val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()

                launch(UI){
                    mProgressDialog.hide()
                }

                if (jsonResponse.toString().contains("\"error\":true")){
                    launch(UI){
                        toast("Error")
                        val response = gson.fromJson(jsonResponse, EkoModel.EkoLookUpFailedResponse::class.java)
                        alert {
                            title = "Validation error"
                            message = response.message
                        }.show()
                    }
                } else {
                    lookupResponse = gson.fromJson(jsonResponse, EkoModel.EkoLookUpSuccessResponse::class.java)
                    launch(UI){
                        alert {
                            title = "Confirm"
                            message = "Name : ${lookupResponse.name}\nAddress : ${lookupResponse.address}"
                            okButton { enterAmount(meterNumber , phoneNumber) }
                        }.show()
                    }
                }
            }
            catch (exception : ConnectException){
                launch(UI){
                    mProgressDialog.dismiss()
                    alert {
                        title = "Response"
                        message = "Error in connection. Please check your internet connection"
                        okButton { }
                    }.show()
                }

            }
            catch (exception : SocketTimeoutException){
                launch(UI){
                    mProgressDialog.dismiss()
                    alert {
                        title = "Response"
                        message = "This connection is taking too long. Please try again"
                    }.show()
                }
            }
            catch (e : retrofit2.HttpException){
                launch(UI){
                    mProgressDialog.dismiss()
                    alert {
                        title = "Error"
                        message = "Error from server. Please try again"
                        okButton {  }
                    }.show()
                }
            }

            catch (e : IllegalStateException){
                launch(UI){
                    mProgressDialog.dismiss()
                    alert {
                        title = "Error"
                        message = "Error from server. Please try again"
                        okButton {  }
                    }.show()
                }
            }

        }
    }

    private fun enterAmount(meter : String, phoneNumber: String) {
        MaterialDialog.Builder(this@EkoPrepaid).title("Enter amount").content("Amount").inputType(InputType.TYPE_CLASS_NUMBER).input("Amount", "") { _, input -> selectTransactionType(meter, phoneNumber, input.toString()) }.show()
    }

    private fun selectTransactionType(meter: String, phoneNumber: String, amount : String){
        meterNumber = meter
        this.phoneNumber = phoneNumber
        this.amount = amount

        val view = LayoutInflater.from(this@EkoPrepaid).inflate(R.layout.activity_enter_pin, null, false)
        alert {
            title = "Transaction Type"
            message = "Select the type of transaction you want to make"
            positiveButton(buttonText = "Card") { _ ->
                isCard = true
                PinAlertUtils.getPin(this@EkoPrepaid, view, this@EkoPrepaid)
            }
            negativeButton(buttonText = "Wallet") {_ ->
                isCard = false

                PinAlertUtils.getPin(this@EkoPrepaid, view, this@EkoPrepaid)
            }
        }.show()
    }

    private fun payWithCard(meter : String, phoneNumber: String, amount: String, pin : String){
        meterNumber = meter
        this.phoneNumber = phoneNumber
        this.amount = amount
        val intent = Intent(this, VasPurchaseProcessor::class.java)
        intent.putExtra(BasePaymentActivity.TRANSACTION_ACCOUNT_TYPE, AccountType.DEFAULT_UNSPECIFIED)

        //amount * 100 to convert the amount to long
        intent.putExtra(BasePaymentActivity.TRANSACTION_AMOUNT,  (amount.toLong() * 100))
        intent.putExtra(BasePaymentActivity.TRANSACTION_ADDITIONAL_AMOUNT, 0L)

        if (SharedPreferenceUtils.getIsTerminalPrepped(this)){
            startActivityForResult(intent, EkoPostpaid.KEYS.EKO_PREPAID_INTENT_CODE)
        } else {
            alert {
                isCancelable = false
                title = "Terminal not configured"
                message = "Click O.K to go to configuration page"
                okButton {
                    startActivity(Intent(this@EkoPrepaid, TermMagmActivity::class.java))
                    //this@EkoPrepaid.finish()
                }
            }.show()
        }


    }

    private fun payWithWallet(meterNumber: String, phoneNumber: String, amount: String, isCard : Boolean, pin : String){
        mProgressDialog.show()
        launch(CommonPool) {

            val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
            val payDetails = if (isCard){
                EkoModel.EkoPayDetails(meter = meterNumber, amount = amount, terminal_id = wallet_id, type = "card", password = wallet_password, user_id = wallet_username, pin = pin, channel = "mobile", lat = SharedPreferenceUtils.getLatitude(this@EkoPrepaid), long = SharedPreferenceUtils.getLongitude(this@EkoPrepaid))
            } else {
                EkoModel.EkoPayDetails(meter = meterNumber, amount = amount, terminal_id = wallet_id, type = "cash", password = wallet_password, user_id = wallet_username, pin = pin, channel = "mobile", lat = SharedPreferenceUtils.getLatitude(this@EkoPrepaid), long = SharedPreferenceUtils.getLongitude(this@EkoPrepaid))
            }

            try {
                val request = EkoService.create().pay(payDetails).await()

                val jsonResponse = Gson().toJsonTree(request).asJsonObject

                launch(UI){
                    mProgressDialog.dismiss()
                }
                if (jsonResponse.toString().contains("\"error\"=true")){
                    val response = gson.fromJson(jsonResponse.toString(), EkoModel.EkoPayFailedResponse::class.java)

                    launch(UI){
                        alert {
                            title = "Response"
                            message = "Error : ${response.message}"

                            okButton { moveToHome() }
                        }.show()
                    }

                } else {
                    val response = gson.fromJson(jsonResponse.toString(), EkoModel.EkoPaySuccessResponse::class.java)
                    if(!response.error){
                        launch(UI){
                            alert {
                                title = "Response"
                                message = "${response.message}"
                                positiveButton(buttonText = "Print"){

                                    if (isCard){
                                        val intent = Intent(this@EkoPrepaid, EkoPrinter::class.java)
                                        intent.putExtra("values", response)
                                        intent.putExtra("extras", BaseReceiptDetails(terminalID = SharedPreferenceUtils.getTerminalId(this@EkoPrepaid)))
                                        intent.putExtra("lookupDetails", lookupResponse)
                                        intent.putExtra("ekedc_type", EkoPrinter.EKEDC_RECEIPT_TYPE.CARD_SUCCESSFUL)
                                        startActivity(intent)
                                        finish()

                                    } else {
                                        val intent = Intent(this@EkoPrepaid, EkoPrinter::class.java)
                                        intent.putExtra("lookupDetails", lookupResponse)
                                        intent.putExtra("values", response)
                                        intent.putExtra("extras", BaseReceiptDetails(terminalID = SharedPreferenceUtils.getTerminalId(this@EkoPrepaid)))
                                        intent.putExtra("ekedc_type", EkoPrinter.EKEDC_RECEIPT_TYPE.CASH_SUCCESSFUL)
                                        startActivity(intent)
                                        finish()

                                    }
                                }

                            }.show()
                        }
                    }else{
                        launch(UI){
                            alert {
                                title = "Response"
                                message = "Error : ${response.message}"
                                okButton { moveToHome() }
                            }.show()
                        }

                    }
                }
            }
            catch (exception : ConnectException){
                launch(UI){
                    mProgressDialog.dismiss()
                    alert {
                        title = "Response"
                        message = "Error in connection. Please check your internet connection"
                        okButton { }
                    }.show()
                }

            }
            catch (exception : SocketTimeoutException){
                launch(UI){
                    mProgressDialog.dismiss()
                    alert {
                        title = "Response"
                        message = "This connection is taking too long. Please try again"
                    }.show()
                }
            }
            catch (e : retrofit2.HttpException){
                launch(UI){
                    mProgressDialog.dismiss()
                    alert {
                        title = "Error"
                        message = "Error from server. Please try again"
                        okButton {  }
                    }.show()
                }
            }
            catch (e : IllegalStateException){
                launch(UI){
                    mProgressDialog.dismiss()
                    alert {
                        title = "Error"
                        message = "Error from server. Please try again"
                        okButton {  }
                    }.show()
                }
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode){
            KEYS.EKO_PREPAID_INTENT_CODE -> when (resultCode){
                Activity.RESULT_OK -> {
                    val state = data?.getSerializableExtra("state") as DeviceState
                    val rrn = data?.getStringExtra("rrn")
                    toast(state.toString())

                    state?.let {
                        when (it){
                            DeviceState.APPROVED -> {
                                mProgressDialog.show()
                                launch(CommonPool){
                                    (application as App).db.transactionResultDao.get(rrn)
                                }
                                payWithWallet(meterNumber, phoneNumber, amount, true, pin)
                            }
                            DeviceState.DECLINED -> {
                                toast("Transaction declined")
                            }
                            DeviceState.FAILED -> {
                                toast("Transaction declined")
                            }
                            else -> {

                            }
                        }

                    }
                }

            }

        }
    }

    private fun moveToHome() {
        finish()
        val intent = Intent(this, EkoElectric::class.java)
        startActivity(intent)
    }

    object KEYS {
       const val EKO_PREPAID_INTENT_CODE = 34324
    }
}
