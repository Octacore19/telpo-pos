package com.iisysgroup.androidlite.cardpaymentprocessors

import android.os.Bundle
import android.util.Log
import com.iisysgroup.androidlite.App
import com.iisysgroup.androidlite.login.TripleDES
import com.iisysgroup.androidlite.payments_menu.BasePaymentActivity
import com.iisysgroup.androidlite.payments_menu.handlers.VasPurchase
import com.iisysgroup.poslib.host.entities.ConfigData
import com.iisysgroup.poslib.host.entities.ConnectionData
import com.iisysgroup.poslib.host.entities.KeyHolder
import com.iisysgroup.poslib.utils.AccountType
import com.iisysgroup.poslib.utils.InputData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.Android
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import org.jetbrains.anko.toast

class VasPurchaseProcessor: BaseCardPaymentProcessor(){
    //Receives amount, additional amount, account type,
    //Returns value to calling library or class


    private val mAmount by lazy {
        intent.getLongExtra(BasePaymentActivity.TRANSACTION_AMOUNT, 0L)
    }

    private val mAdditionalAmount by lazy {
        intent.getLongExtra(BasePaymentActivity.TRANSACTION_ADDITIONAL_AMOUNT, 0L)
    }

    private val mAccountType by lazy {
        intent.getSerializableExtra(BasePaymentActivity.TRANSACTION_ACCOUNT_TYPE) as AccountType
    }


    override fun initializeDefaultUI(): DefaultUIModel {
        return DefaultUIModel(transactionTitle = "VAS Purchase", amount = mAmount)
    }

    private val mDb by lazy {
        (application as App).db
    }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Single.fromCallable {
            mDb.vasTerminalDataDao.get()
        }.subscribeOn(Schedulers.io())
                .flatMap { vasTerminalDetails ->
            val connectionData = ConnectionData(vasTerminalDetails.tid, "196.6.103.73", 5000, false)

            val mastekey = "F0BB4B6F425CE3CDF10BAB787563D328"
            val ssesionKey = TripleDES.encryptKey(vasTerminalDetails.sessionKey, mastekey);
            val pinKey = TripleDES.encryptKey(vasTerminalDetails.pinKey, mastekey)
            val keyHolder = KeyHolder(mastekey, ssesionKey, pinKey)

            val configData = ConfigData()


            //Time out
            configData.storeConfigData("04002", "90")

            //Country Code
            configData.storeConfigData("06003", vasTerminalDetails.countryCode)

            //MCC
            configData.storeConfigData("08004", vasTerminalDetails.mcc)


            //Merchant's name - 40 length
            configData.storeConfigData("52040", vasTerminalDetails.merchantName)

            //Merchant Id - 15 length
            configData.storeConfigData("03015", vasTerminalDetails.mid)

            //Currency Code
            configData.storeConfigData("05003", vasTerminalDetails.currencyCode)

            val inputData = InputData(mAmount, mAdditionalAmount, mAccountType)
            val vasPurchase = VasPurchase(this, mDb, inputData, mHostInteractor, connectionData, mEmvInteractor, configData, keyHolder)

            vasPurchase.getTransactionResult()
        }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result, error ->

                        result?.let {
                            Log.d("OkHTransactionResult",  it.toString())
                            mEmvInteractor.processOnlineResponse(it.responseCode, it.issuerAuthData91, it.issuerScript71, it.issuerScript72)
                            setTransactionRrn(it.RRN)
                            DbManager(application).saveTransactionData(it)

                        }

                    error?.let {
                        toast(it.message!!)

                    }
                    }


    }

}
