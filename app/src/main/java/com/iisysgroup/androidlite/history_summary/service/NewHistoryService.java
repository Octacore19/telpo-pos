package com.iisysgroup.androidlite.history_summary.service;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.iisysgroup.androidlite.history_summary.model.TrackHistoryModel;
import com.iisysgroup.androidlite.history_summary.model.TransactionHistoryModel;
import com.iisysgroup.androidlite.utils.SharedPreferenceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NewHistoryService {
    Context cont;
    SharedPreferenceUtils sharedPreferenceUtils;
    public NewHistoryService(Context context){
        this.cont = context;
        sharedPreferenceUtils = SharedPreferenceUtils.INSTANCE;
    }
   public TrackHistoryModel getFirstPage() throws IOException {
       TrackHistoryModel result = new TrackHistoryModel();
       OkHttpClient client = new OkHttpClient();

       MediaType mediaType = MediaType.parse("application/json");
       RequestBody body = RequestBody.create(mediaType, "{\"wallet\":\""+sharedPreferenceUtils.getPayviceWalletId(cont)+
               "\",\n\"username\": \""+sharedPreferenceUtils.getPayviceUsername(cont)+
               "\",\n\t\"password\" : \""+sharedPreferenceUtils.getPayvicePassword(cont)+"\",\"limit\" : \"200\"\n}");
       Request request = new Request.Builder()
               .url("http://basehuge.itexapp.com:8090/api/account/transaction-history")
               .post(body)
               .addHeader("content-type", "application/json")
               .addHeader("cache-control", "no-cache")
               .build();

       Response response = client.newCall(request).execute();
       String responseBody = response.body().string();
       Log.i("okh2", responseBody);
       try {
           JSONObject jsonObject = new JSONObject(responseBody);
           if(jsonObject.getBoolean("error")){
               result.setError(true);
               Log.i("okh2", "First Error");
               result.setMessage(jsonObject.getString("message"));

           }else{
               JSONObject obj = jsonObject.getJSONObject("userTransactions");

               JSONArray array = new JSONArray(obj.getString("transactions"));
               List<TransactionHistoryModel> transactionHistoryModels = new ArrayList<>();
               Log.i("okh23", array.length() + "");
               Gson gson = new Gson();
               try{
                   boolean yes = true;
                   int index = 0;
                   while (yes){
                       transactionHistoryModels.add(gson.fromJson(array.getJSONObject(index).toString(), TransactionHistoryModel.class));
                       if(index < array.length() - 1){
                           index++;
                       }else{
                           yes = false;
                       }
                   }
               }catch (Exception e){
                   e.printStackTrace();
               }

               Log.i("okh23", transactionHistoryModels.size() + "");
               result.setError(false);
               result.setMessage(jsonObject.getString("message"));
               result.setCurrentPage(jsonObject.getJSONObject("userTransactions").getJSONObject("transactionSummarry").getInt("currentPage"));
               result.setLastPage(jsonObject.getJSONObject("userTransactions").getJSONObject("transactionSummarry").getInt("lastPage"));
               result.setTransactionHistoryModelList(transactionHistoryModels);
               return result;

           }
       } catch (JSONException e) {
           e.printStackTrace();
           result.setError(true);
           result.setMessage("Failed to get your transaction history");
       }

       return result;
   }

    public TrackHistoryModel getNextPage(int page) throws IOException {
        TrackHistoryModel result = new TrackHistoryModel();
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\"wallet\":\""+sharedPreferenceUtils.getPayviceWalletId(cont) +"\",\n\"username\": \""+sharedPreferenceUtils.getPayviceUsername(cont)+"\",\n\t\"password\" : \""+sharedPreferenceUtils.getPayvicePassword(cont)+"\",\"limit\" : \""+page+"\"\n}");
        Request request = new Request.Builder()
                .url("http://basehuge.itexapp.com:8090/api/account/transaction-history")
                .post(body)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();
        String responseBody = response.body().string();
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            if(jsonObject.getBoolean("error")){
                result.setError(true);
                result.setMessage(jsonObject.getString("message"));

            }else{
                JSONArray array = jsonObject.getJSONObject("userTransactions").getJSONArray("transactions");
                List<TransactionHistoryModel> transactionHistoryModels = new ArrayList<>();
                Gson gson = new Gson();
                for(int i = 0; i < array.length(); i++){
                    transactionHistoryModels.add(gson.fromJson(array.getJSONObject(i).toString(), TransactionHistoryModel.class));
                }
                result.setError(false);
                result.setMessage(jsonObject.getString("message"));
                result.setCurrentPage(jsonObject.getJSONObject("userTransactions").getJSONObject("transactionSummarry").getInt("currentPage"));
                result.setLastPage(jsonObject.getJSONObject("userTransactions").getJSONObject("transactionSummarry").getInt("lastPage"));
                result.setTransactionHistoryModelList(transactionHistoryModels);
                return result;

            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.setError(true);
            result.setMessage("Failed to get your transaction history");
        }

        return result;
    }
}
