package com.iisysgroup.androidlite

import android.Manifest
import android.content.Intent
import android.graphics.*
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import com.iisysgroup.androidlite.models.ReceiptModel
import com.iisysgroup.androidlite.utils.PrintUtils
import com.iisysgroup.androidlite.utils.SharedPreferenceUtils
import com.iisysgroup.poslib.deviceinterface.printer.BitmapPrintable
import com.iisysgroup.poslib.deviceinterface.printer.PrintFormat
import com.iisysgroup.poslib.deviceinterface.printer.PrinterState
import com.iisysgroup.poslib.deviceinterface.printer.StringPrintable
//import com.telpo.moduled.Telpo900Device
import kotlinx.android.synthetic.main.activity_print.*
import kotlinx.android.synthetic.main.activity_print.view.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import java.io.ByteArrayOutputStream
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import com.iisysgroup.androidlite.utils.PrintUtils.printBitmap
import com.iisysgroup.androidlite.utils.sendNotify
import com.iisysgroup.newland.NewlandDevice
import com.telpo.moduled.Telpo900Device
import org.jetbrains.anko.cancelButton


class PrintActivity : AppCompatActivity() {

    enum class VasType {
       AIRTEL_VTU, AIRTEL_DATA, MTN_VTU, MTN_DATA, GLO_VTU, GLO_DATA, ETISALAT_VTU, ETISALAT_DATA, SMILE_TOP_UP, SMILE_DATA_PURCHASE, DSTV, GOTV, EKEDC_PREPAID, EKEDC_POSTPAID, IKEDC_PREPAID, IKEDC_POSTPAID, NOT_INCLUDED, PURCHASE
    }

     val telpo900Device by lazy {
//         NewlandDevice(this)
         Telpo900Device(this)
     };

    val TAG = "MYDEBUG"
    val PRINTER_WIDTH = 384

    object KEYS {
        const val PRINT_RECEIPT_VAS_TYPE = "vas_type"
        const val PRINT_RECEIPT_MODEL_KEY = "print_map"

        const val PRINT_RECEIPT_RECEIPT_OWNER = "receipt_owner"
        const val PRINT_RECEIPT_TERMINAL_ID = "receipt_terminal_id"
    }

    private var vasType : VasType?  = null

    private val printMap by lazy {
        intent.getSerializableExtra(KEYS.PRINT_RECEIPT_MODEL_KEY) as ReceiptModel
    }

    private val configData by lazy {
        (application as App).db.configDataDao
    }

    private val terminalId by lazy {
        if (intent.hasExtra(KEYS.PRINT_RECEIPT_TERMINAL_ID)){
            intent.getStringExtra(KEYS.PRINT_RECEIPT_TERMINAL_ID)
        } else {
            SharedPreferenceUtils.getTerminalId(this)
        }

    }

    private val isOneTimePrint by lazy {
        intent.hasExtra(KEYS.PRINT_RECEIPT_RECEIPT_OWNER)
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_print)
        val receipt = findViewById<LinearLayout>(R.id.receipt)

        launch {
            val merchantName =  configData.get().getConfigData("52040")
            val  mid = configData.get().getConfigData("03015").toString()
            launch(UI){
                receipt.merchantName.text = merchantName.toString()


                if (intent.hasExtra(KEYS.PRINT_RECEIPT_VAS_TYPE)){
                    vasType = intent.getSerializableExtra(KEYS.PRINT_RECEIPT_VAS_TYPE) as VasType

                    if (vasType == VasType.PURCHASE){
                        val drawable = PrintUtils.getDrawableFromTerminalId(this@PrintActivity, terminalId)
                        receipt.bankImage.setImageDrawable(drawable)
                        sendNotify(printMap.map.get("RRN")!!)
                    } else {
                        val drawable = PrintUtils.getDrawableFromVasType(this@PrintActivity, vasType)
                         if(printMap.map.get("RRN")!= null){
                             sendNotify(printMap.map.get("RRN")!!, vasType.toString().replace("_", ""),vasType.toString().replace("_", ""))
                         }else{
                             sendNotify(vasType.toString().replace("_", ""),vasType.toString().replace("_", ""),printMap.amount.toFloat().toInt(),this@PrintActivity,
                                     if(!printMap.transactionStatus.equals("Approved")){"06"}else{"00"}, mid)
                         }
                        receipt.bankImage.setImageDrawable(drawable)
                    }
                } else {
                    vasType = VasType.NOT_INCLUDED
                    val drawable = getDrawable(R.drawable.itex)
                    receipt.bankImage.setImageDrawable(drawable)
                    if(printMap.map.get("RRN")!= null){
                        sendNotify(printMap.map.get("RRN")!!, vasType.toString().replace("_", ""),vasType.toString().replace("_", ""))
                    }else{
                        sendNotify(vasType.toString().replace("_", ""),vasType.toString().replace("_", ""),printMap.amount.toFloat().toInt(),this@PrintActivity,if(!printMap.transactionStatus.equals("Approved")){"06"}else{"00"},mid)
                    }
                }

                if (isOneTimePrint){
                    receipt.receiptOwner.text = intent.getStringExtra(KEYS.PRINT_RECEIPT_RECEIPT_OWNER)
                    val view = PrintUtils.generateReceipt(this@PrintActivity, printMap, receipt)


                    view.post {
                        val bitmap = getBitmapFromView(view)
                        printBitmap(bitmap, true)
//                        printOrEmail(false,view)
                    }
                } else {
                    receipt.receiptOwner.text = "Customer's copy"

                    val view = PrintUtils.generateReceipt(this@PrintActivity, printMap, receipt)
                    view.post {
                        val bitmap = getBitmapFromView(view)
                        printBitmap(bitmap, true)
//                        printOrEmail(false, view)
                        alert {
                            title = "Merchant's copy"
                            message = "Press OK to print Merchant's copy"
                            okButton {
                                receipt.receiptOwner.text = "Merchant's copy"
                                val bitmapMerchants = getBitmapFromView(view)
                                printBitmap(bitmapMerchants, true)
                            }
                            cancelButton {

                            }
                        }.show()
                    }
                }
            }
        }
    }
    

//
//    fun  printOrEmail(email: Boolean, view: View){
//        if(email){
//            alert {
//                title = "Merchant's copy"
//                message = "Press OK to print Merchant's copy"
//                okButton {
//                    receipt.receiptOwner.text = "Merchant's copy"
//                    val bitmap = getBitmapFromView(view)
//                    printBitmap(bitmap, true)
//                }
//            }.show()
//        }else{
//            if(isStoragePermissionGranted()) {
//                alert {
//                    title = "Email Receipt"
//                    message = "Press OK to to Email the recipt"
//                    okButton {
//                        //receipt.receiptOwner.text = "Merchant's copy"
//                        finish()
//                        val bitmap = getBitmapFromView(view)
//                        val shareIntent = Intent(Intent.ACTION_SEND)
//                        shareIntent.setType("image/*")
//                        val uri = Uri.parse(MediaStore.Images.Media.insertImage(contentResolver, bitmap, "Receipt", null))
//                        val mUsername = SharedPreferenceUtils.getPayviceUsername(this@PrintActivity)
//
//                        shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
//                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Purchase");
//                        shareIntent.putExtra(Intent.EXTRA_CC, mUsername);
//                        val  status = printMap.transactionStatus
//                        val merchantName = printMap.map.get("Card Holder")
//
//                        Log.d("details", "username" + mUsername + " status" + status + "merchantName "+merchantName)
//
//                        shareIntent.putExtra(Intent.EXTRA_TEXT   , "Hello, "+ merchantName+ " your purchase of ₦"+ printMap.amount+" was "+ status + ".\n" + "Please find the transaction receipt in this mail attachment");
//                        startActivity(Intent.createChooser(shareIntent, "Send Receipt"))
//
//                        //printBitmap(bitmap, true)
//                    }
//                }.show()
//            }else{
//                ActivityCompat.requestPermissions(this,  arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1000);
//
//            }
//        }
//    }
    

    fun getBitmapFromView(view: View): Bitmap {
        //Define a bitmap with the same size as the view
        val returnedBitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        //Bind a canvas to it
        val canvas = Canvas(returnedBitmap)
        //Get the view's background
        val bgDrawable = view.background
        if (bgDrawable != null)
        //has background drawable , then draw it on the canvas
            bgDrawable.draw(canvas)
        else
        //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE)
        // draw the view on the canvas
        view.draw(canvas)
        //return the bitmap
        return returnedBitmap
    }


    private fun printBitmap(bitmap: Bitmap?, fitToPage: Boolean) {
        if (bitmap == null || bitmap.width == 0 || bitmap.height == 0) {
            return
        }

        Log.d(TAG, "original Bitmap Width:" + bitmap.width + "   Height:" + bitmap.height + "  FitToPage=" + fitToPage)




        if (fitToPage) {
            val scaledHeight = PRINTER_WIDTH * bitmap.height / bitmap.width
            val scaledBitmap = Bitmap.createScaledBitmap(bitmap, PRINTER_WIDTH, scaledHeight * 2 , false)
            Log.d(TAG, "scaled Bitmap Width:" + scaledBitmap.width + "   Height:" + scaledBitmap.height *2)
            // Draw the bitmap
            val bitmapPrintable = BitmapPrintable(scaledBitmap, PrintFormat(PrintFormat.Align.CENTER, scaledHeight.toFloat(), scaledBitmap.width.toFloat()))
            var list = listOf(bitmapPrintable)
            telpo900Device.print(list,object : com.iisysgroup.poslib.deviceinterface.Printer.PrinterCallback {
                override fun onNotifyPrinterStatus(p0: PrinterState?) {
                     when(p0){
                         PrinterState.ERROR -> alert {
                             message = "Error with printer"
                             positiveButton(buttonText = "OK", onClicked = {
                                 it.dismiss()
                             })
                         }

                         PrinterState.NO_PAPER -> alert {
                             message = "No Paper"
                             positiveButton(buttonText = "OK", onClicked = {
                                 it.dismiss()
                             })
                         }
                     }
                }

            })
        } else {
            // Draw the bitmap
            val bitmapPrintable = BitmapPrintable(bitmap, PrintFormat(PrintFormat.Align.CENTER, bitmap.height.toFloat(), bitmap.width.toFloat()))
            var list = List(1,{bitmapPrintable})
            telpo900Device.print(list,object : com.iisysgroup.poslib.deviceinterface.Printer.PrinterCallback {
                override fun onNotifyPrinterStatus(p0: PrinterState?) {
                    when(p0){
                        PrinterState.ERROR -> alert {
                            message = "Error with printer"
                            positiveButton(buttonText = "OK", onClicked = {
                                it.dismiss()
                            })
                        }

                        PrinterState.NO_PAPER -> alert {
                            message = "No Paper"
                            positiveButton(buttonText = "OK", onClicked = {
                                it.dismiss()
                            })
                        }
                    }
                }

            })

        }


    }

    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v("okh", "Permission is granted")
                return true
            } else {

                Log.v("okh", "Permission is revoked")
                ActivityCompat.requestPermissions(this, arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                return false
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("okh", "Permission is granted")
            return true
        }
    }



}
