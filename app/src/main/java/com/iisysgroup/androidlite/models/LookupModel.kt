package com.iisysgroup.androidlite.models

data class LookupSuccessModel(val status : Int, val message : String, val beneficiaryName : String, val vendorBankCode : String, val convenienceFee : Int)

data class LookupFailedModel(val status : Int, val message : String)

//@SerializedName("GetAccountInGTBResult") val result : GetAccountInGTBResult

/*data class GetAccountInGTBResult(@SerializedName("Response") val response : Response)

data class Response(@SerializedName("CODE")val CODE: String, @SerializedName("ACCOUNTNAME") val ACCOUNTNAME: String)*/

