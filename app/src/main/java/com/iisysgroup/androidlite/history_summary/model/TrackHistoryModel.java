package com.iisysgroup.androidlite.history_summary.model;

import java.util.List;

public class TrackHistoryModel {
    private List<TransactionHistoryModel> transactionHistoryModelList;
    private int currentPage;
    private  int lastPage;
    private boolean error;
    private String message;

    public List<TransactionHistoryModel> getTransactionHistoryModelList() {
        return transactionHistoryModelList;
    }

    public void setTransactionHistoryModelList(List<TransactionHistoryModel> transactionHistoryModelList) {
        this.transactionHistoryModelList = transactionHistoryModelList;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getLastPage() {
        return lastPage;
    }

    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
