package com.iisysgroup.androidlite.utils;

import android.util.Log;

/**
 * Created by Agbede on 3/14/2018.
 */

public class StringUtils {
    public static String getPrintableLine(String a, String b){
        int length = 32;
        int a_length = a.length();
        int b_length = b.length();

        int total_length = a_length + b_length;
        int space_length = length - total_length;

        StringBuilder stringBuilder = new StringBuilder(32);
        stringBuilder.append(a);
        for (int i = 0; i < space_length; i++){
            stringBuilder.append(" ");
        }
        stringBuilder.append(b);

        Log.d("Final string", stringBuilder.toString());

        return stringBuilder.toString();
    }
}
