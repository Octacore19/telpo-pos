package com.iisysgroup.androidlite.history_summary.model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionHistoryModel implements Serializable
{
    @SerializedName("reference")
    @Expose
    private String reference;
    @SerializedName("terminalIRN")
    @Expose
    private String terminalIRN;
    @SerializedName("account")
    @Expose
    private String account;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("paymentMethod")
    @Expose
    private String paymentMethod;
    @SerializedName("pan")
    @Expose
    private Object pan;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("reversed")
    @Expose
    private Boolean reversed;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("dateSentence")
    @Expose
    private String dateSentence;
    @SerializedName("merchantIRN")
    @Expose
    private String merchantIRN;
    @SerializedName("productReference")
    @Expose
    private String productReference;
    @SerializedName("internalAuditReference")
    @Expose
    private String internalAuditReference;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("beneficiary")
    @Expose
    private String beneficiary;
    @SerializedName("reversalReference")
    @Expose
    private Object reversalReference;
    @SerializedName("auditReference")
    @Expose
    private String auditReference;
    @SerializedName("balanceReference")
    @Expose
    private String balanceReference;
    @SerializedName("auditAccountReference")
    @Expose
    private String auditAccountReference;
    @SerializedName("auditAmount")
    @Expose
    private String auditAmount;
    @SerializedName("auditDescription")
    @Expose
    private String auditDescription;
    @SerializedName("auditUserReference")
    @Expose
    private String auditUserReference;
    @SerializedName("auditBalanceReference")
    @Expose
    private Object auditBalanceReference;
    @SerializedName("balanceAfter")
    @Expose
    private String balanceAfter;
    @SerializedName("commissionReference")
    @Expose
    private String commissionReference;
    @SerializedName("commissionAmount")
    @Expose
    private String commissionAmount;
    @SerializedName("originalTransactionReference")
    @Expose
    private String originalTransactionReference;
    @SerializedName("originalTransactionAmount")
    @Expose
    private String originalTransactionAmount;
    @SerializedName("originalTransactionCategory")
    @Expose
    private Object originalTransactionCategory;
    @SerializedName("originalTransactionType")
    @Expose
    private Object originalTransactionType;
    @SerializedName("originalTransactionProduct")
    @Expose
    private Object originalTransactionProduct;
    @SerializedName("originalTransactionDescription")
    @Expose
    private Object originalTransactionDescription;
    @SerializedName("walletID")
    @Expose
    private String walletID;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private Object phone;
    @SerializedName("superAgentWalletID")
    @Expose
    private Object superAgentWalletID;
    private final static long serialVersionUID = 7155042658268419431L;

    /**
     * No args constructor for use in serialization
     *
     */
    public TransactionHistoryModel() {
    }

    /**
     *
     * @param balanceAfter
     * @param phone
     * @param terminalIRN
     * @param originalTransactionDescription
     * @param originalTransactionCategory
     * @param balanceReference
     * @param date
     * @param type
     * @param dateSentence
     * @param commissionAmount
     * @param beneficiary
     * @param amount
     * @param auditAmount
     * @param auditReference
     * @param description
     * @param name
     * @param originalTransactionProduct
     * @param reversed
     * @param internalAuditReference
     * @param originalTransactionAmount
     * @param auditBalanceReference
     * @param commissionReference
     * @param status
     * @param superAgentWalletID
     * @param reference
     * @param pan
     * @param product
     * @param category
     * @param merchantIRN
     * @param walletID
     * @param originalTransactionType
     * @param auditAccountReference
     * @param email
     * @param reversalReference
     * @param originalTransactionReference
     * @param account
     * @param auditDescription
     * @param auditUserReference
     * @param productReference
     * @param user
     * @param paymentMethod
     */
    public TransactionHistoryModel(String reference, String terminalIRN, String account, String product, String category, String amount, String description, String paymentMethod, Object pan, Boolean status, Boolean reversed, String date, String dateSentence, String merchantIRN, String productReference, String internalAuditReference, String type, String beneficiary, Object reversalReference, String auditReference, String balanceReference, String auditAccountReference, String auditAmount, String auditDescription, String auditUserReference, Object auditBalanceReference, String balanceAfter, String commissionReference, String commissionAmount, String originalTransactionReference, String originalTransactionAmount, Object originalTransactionCategory, Object originalTransactionType, Object originalTransactionProduct, Object originalTransactionDescription, String walletID, String name, String user, String email, Object phone, Object superAgentWalletID) {
        super();
        this.reference = reference;
        this.terminalIRN = terminalIRN;
        this.account = account;
        this.product = product;
        this.category = category;
        this.amount = amount;
        this.description = description;
        this.paymentMethod = paymentMethod;
        this.pan = pan;
        this.status = status;
        this.reversed = reversed;
        this.date = date;
        this.dateSentence = dateSentence;
        this.merchantIRN = merchantIRN;
        this.productReference = productReference;
        this.internalAuditReference = internalAuditReference;
        this.type = type;
        this.beneficiary = beneficiary;
        this.reversalReference = reversalReference;
        this.auditReference = auditReference;
        this.balanceReference = balanceReference;
        this.auditAccountReference = auditAccountReference;
        this.auditAmount = auditAmount;
        this.auditDescription = auditDescription;
        this.auditUserReference = auditUserReference;
        this.auditBalanceReference = auditBalanceReference;
        this.balanceAfter = balanceAfter;
        this.commissionReference = commissionReference;
        this.commissionAmount = commissionAmount;
        this.originalTransactionReference = originalTransactionReference;
        this.originalTransactionAmount = originalTransactionAmount;
        this.originalTransactionCategory = originalTransactionCategory;
        this.originalTransactionType = originalTransactionType;
        this.originalTransactionProduct = originalTransactionProduct;
        this.originalTransactionDescription = originalTransactionDescription;
        this.walletID = walletID;
        this.name = name;
        this.user = user;
        this.email = email;
        this.phone = phone;
        this.superAgentWalletID = superAgentWalletID;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTerminalIRN() {
        return terminalIRN;
    }

    public void setTerminalIRN(String terminalIRN) {
        this.terminalIRN = terminalIRN;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Object getPan() {
        return pan;
    }

    public void setPan(Object pan) {
        this.pan = pan;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getReversed() {
        return reversed;
    }

    public void setReversed(Boolean reversed) {
        this.reversed = reversed;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateSentence() {
        return dateSentence;
    }

    public void setDateSentence(String dateSentence) {
        this.dateSentence = dateSentence;
    }

    public String getMerchantIRN() {
        return merchantIRN;
    }

    public void setMerchantIRN(String merchantIRN) {
        this.merchantIRN = merchantIRN;
    }

    public String getProductReference() {
        return productReference;
    }

    public void setProductReference(String productReference) {
        this.productReference = productReference;
    }

    public String getInternalAuditReference() {
        return internalAuditReference;
    }

    public void setInternalAuditReference(String internalAuditReference) {
        this.internalAuditReference = internalAuditReference;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(String beneficiary) {
        this.beneficiary = beneficiary;
    }

    public Object getReversalReference() {
        return reversalReference;
    }

    public void setReversalReference(Object reversalReference) {
        this.reversalReference = reversalReference;
    }

    public String getAuditReference() {
        return auditReference;
    }

    public void setAuditReference(String auditReference) {
        this.auditReference = auditReference;
    }

    public String getBalanceReference() {
        return balanceReference;
    }

    public void setBalanceReference(String balanceReference) {
        this.balanceReference = balanceReference;
    }

    public String getAuditAccountReference() {
        return auditAccountReference;
    }

    public void setAuditAccountReference(String auditAccountReference) {
        this.auditAccountReference = auditAccountReference;
    }

    public String getAuditAmount() {
        return auditAmount;
    }

    public void setAuditAmount(String auditAmount) {
        this.auditAmount = auditAmount;
    }

    public String getAuditDescription() {
        return auditDescription;
    }

    public void setAuditDescription(String auditDescription) {
        this.auditDescription = auditDescription;
    }

    public String getAuditUserReference() {
        return auditUserReference;
    }

    public void setAuditUserReference(String auditUserReference) {
        this.auditUserReference = auditUserReference;
    }

    public Object getAuditBalanceReference() {
        return auditBalanceReference;
    }

    public void setAuditBalanceReference(Object auditBalanceReference) {
        this.auditBalanceReference = auditBalanceReference;
    }

    public String getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(String balanceAfter) {
        this.balanceAfter = balanceAfter;
    }

    public String getCommissionReference() {
        return commissionReference;
    }

    public void setCommissionReference(String commissionReference) {
        this.commissionReference = commissionReference;
    }

    public String getCommissionAmount() {
        return commissionAmount;
    }

    public void setCommissionAmount(String commissionAmount) {
        this.commissionAmount = commissionAmount;
    }

    public String getOriginalTransactionReference() {
        return originalTransactionReference;
    }

    public void setOriginalTransactionReference(String originalTransactionReference) {
        this.originalTransactionReference = originalTransactionReference;
    }

    public String getOriginalTransactionAmount() {
        return originalTransactionAmount;
    }

    public void setOriginalTransactionAmount(String originalTransactionAmount) {
        this.originalTransactionAmount = originalTransactionAmount;
    }

    public Object getOriginalTransactionCategory() {
        return originalTransactionCategory;
    }

    public void setOriginalTransactionCategory(Object originalTransactionCategory) {
        this.originalTransactionCategory = originalTransactionCategory;
    }

    public Object getOriginalTransactionType() {
        return originalTransactionType;
    }

    public void setOriginalTransactionType(Object originalTransactionType) {
        this.originalTransactionType = originalTransactionType;
    }

    public Object getOriginalTransactionProduct() {
        return originalTransactionProduct;
    }

    public void setOriginalTransactionProduct(Object originalTransactionProduct) {
        this.originalTransactionProduct = originalTransactionProduct;
    }

    public Object getOriginalTransactionDescription() {
        return originalTransactionDescription;
    }

    public void setOriginalTransactionDescription(Object originalTransactionDescription) {
        this.originalTransactionDescription = originalTransactionDescription;
    }

    public String getWalletID() {
        return walletID;
    }

    public void setWalletID(String walletID) {
        this.walletID = walletID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getPhone() {
        return phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    public Object getSuperAgentWalletID() {
        return superAgentWalletID;
    }

    public void setSuperAgentWalletID(Object superAgentWalletID) {
        this.superAgentWalletID = superAgentWalletID;
    }

}




