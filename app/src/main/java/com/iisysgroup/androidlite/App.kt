package com.iisysgroup.androidlite

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.location.LocationManager
import android.preference.PreferenceManager
import android.provider.Settings
import android.support.v7.app.AlertDialog
//import com.google.firebase.analytics.FirebaseAnalytics
import com.iisysgroup.androidlite.db.BeneficiariesDatabase
import com.iisysgroup.androidlite.login.securestorage.SecureStorage
import com.iisysgroup.androidlite.utils.OnLocation
import com.iisysgroup.poslib.ISO.GTMS.GtmsHost
import com.iisysgroup.poslib.ISO.POSVAS.PosvasHost
import com.iisysgroup.poslib.TAMS.TamsHost
import com.iisysgroup.poslib.host.HostInteractor
import com.iisysgroup.poslib.host.dao.PosLibDatabase
import com.iisysgroup.poslib.host.entities.ConfigData
import net.danlew.android.joda.JodaTimeAndroid
import org.jetbrains.anko.alert


/**
 * Created by Agbede on 2/26/2018.
 */
class App : Application() {



    val TAG = "app_pit_ogl"

    val db by lazy {
        Room.databaseBuilder(this, PosLibDatabase::class.java, "poslib.db")
                .fallbackToDestructiveMigration()
                .build()
    }

    val beneficiariesDatabase by lazy {
        Room.databaseBuilder(this, BeneficiariesDatabase::class.java, "beneficiaries.db")
                .fallbackToDestructiveMigration()
                .build()
    }

    private val sharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(this)
    }


    val hostInteractor: HostInteractor by lazy {
        val hostKey = getString(R.string.key_host_type)

        val host = when (sharedPreferences.getString(hostKey, "")) {
            "TAMS" -> TamsHost(this)
            "POSVAS" -> PosvasHost(this)
            else -> GtmsHost(this)
        }

        HostInteractor.getInstance(host)
    }

    override fun onCreate() {
        super.onCreate()

        JodaTimeAndroid.init(this)

        SecureStorage.init(this)
                .setEncryptionMethod(SecureStorage.Builder.EncryptionMethod.ENCRYPTED)
                .setPassword("4321dcbA")
                .setStoreName(TAG)
                .build()


    }







}