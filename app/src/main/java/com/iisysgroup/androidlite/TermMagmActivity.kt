package com.iisysgroup.androidlite

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import com.google.gson.Gson
import com.iisysgroup.androidlite.db.VasTerminalService
import com.iisysgroup.androidlite.utils.SharedPreferenceUtils
import com.iisysgroup.poslib.host.HostInteractor
import com.iisysgroup.poslib.host.dao.PosLibDatabase
import com.iisysgroup.poslib.host.entities.ConfigData
import com.iisysgroup.poslib.host.entities.ConnectionData
import com.iisysgroup.poslib.host.entities.KeyHolder
import com.iisysgroup.poslib.host.entities.VasTerminalData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_term_magm.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.okButton
import org.jetbrains.anko.toast


class TermMagmActivity : AppCompatActivity() {

    object TerminalUtils {

        fun isTerminalPrepped(context: Context, application : App) : Boolean {
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

            val isValidSettings = when {
                !sharedPreferences.contains(context.getString(R.string.key_terminal_id)) ||
                        !sharedPreferences.contains(context.getString(R.string.key_ip_address)) ||
                        !sharedPreferences.contains(context.getString(R.string.key_ip_address)) ||
                        !sharedPreferences.contains(context.getString(R.string.key_pref_port_type)) ||
                        !sharedPreferences.contains(context.getString(R.string.key_host_type)) -> false
                else -> true
            }

            if (!isValidSettings){
                return false
            } else {
                (application).db.configDataDao.get() ?: return false
            }

            return true
        }
    }

    private fun SharedPreferences.isSSL() : Boolean {
        return when (this.getString(getString(R.string.key_host_type), "")){
            "" -> false
            "SSL" -> true
            else -> true
        }
    }

    private val sharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(this)
    }

    private val connectionData by lazy {
        val terminal_id = sharedPreferences.getString(getString(R.string.key_terminal_id), null)
        val hostType =sharedPreferences.getString(getString(R.string.key_host_type), null)
//        val ip_address = sharedPreferences.getString(getString(R.string.key_ip_address), null)
//        val ip_port = Integer.parseInt(sharedPreferences.getString(getString(R.string.key_pref_port), null))
        var ip_address = ""
        var ip_port = 0
        val isSSL = sharedPreferences.isSSL()

//        if (terminal_id.equals(resources.getResourceName(R.string.def_terminal_id))){
//            alert{
//                title="Terminal ID not set"
//                message="Click Ok to move to settings page"
//                isCancelable=false
//                okButton {
//                    startActivity(Intent(this@TermMagmActivity,SettingsActivity::class.java))
//                }
//            }
//        }

        when (hostType) {

            "EPMS" -> {
                Log.d("pref_changed_element  >>>>", hostType)

                ip_port=5043
                ip_address ="196.6.103.73"
            }
            "POSVAS" -> {
                Log.d("pref_changed_element  >>>>", "POSVAS")

                ip_port=5003
                ip_address ="197.253.19.75"
            }


            else -> {
                Log.d("pref_changed_element  >>>>", "TAMS")

//                port_EditTextPreference.setSummary("000")
//                ip_EditTextPreference.setSummary("00000")

            }
        }


        ConnectionData(terminal_id, ip_address, ip_port, isSSL)


    }

    val hostInteractor: HostInteractor by lazy {
        (application as App).hostInteractor
    }

    fun displayError(e: Exception){
        progressDialog.dismiss()
        alert.setTitle("Error")
        alert.setMessage(e.message)
        alert.show()

    }

    val alert by lazy{
        AlertDialog.Builder(this)
                .setTitle(null)
                .setMessage(null)
                .create()
    }

    val db by lazy {
        (application as App).db
    }

    val progressDialog by lazy {
        ProgressDialog(this).apply { setCancelable(false) }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_term_magm)

        setSupportActionBar(toolbar)

        supportActionBar?.let {
            //            it.setDisplayHomeAsUpEnabled(true)
        }

        configureTerminalBtn.setOnClickListener {
            configureTerminal()
        }
        downloadParameterBtn.setOnClickListener {
            downloadParameters()
        }

        if (!isValidConnectionSettings())
        {
            alert {
                title = getString(R.string.app_name)
                message = "No settings set. Press OK to update your settings"
                isCancelable = false
                okButton {
                    startActivity(Intent(this@TermMagmActivity, SettingsActivity::class.java))
                }
            }.show()
        }
    }

    private fun isValidConnectionSettings() = when {
        !sharedPreferences.contains(getString(R.string.key_terminal_id)) ||
                !sharedPreferences.contains(getString(R.string.key_ip_address)) ||
                !sharedPreferences.contains(getString(R.string.key_ip_address)) ||
                !sharedPreferences.contains(getString(R.string.key_pref_port_type)) ||
                !sharedPreferences.contains(getString(R.string.key_host_type)) -> false
        else -> true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when(item?.itemId){
            android.R.id.home -> this.onBackPressed()
        }
        return true
    }
    override fun onBackPressed() {
        super.onBackPressed()
        NavUtils.navigateUpFromSameTask(this)
    }
    private fun downloadParameters()
    {
        launch(CommonPool){
            val keyHolder = db.keyHolderDao.get()

            if (keyHolder == null)
            {
                launch (UI){
                    alert {
                        title = "Error"
                        message = "Invalid keys. Press OK to reconfigure terminal"
                        isCancelable = false
                        okButton { configureTerminal()
                        }
                    }.show()
                }
            }
            else {
                launch(UI){
                    progressDialog.setMessage("Now downloading parameters")
                    progressDialog.show()
                }


                hostInteractor.getConfigData(connectionData, keyHolder)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {configData: ConfigData?, throwable: Throwable? ->
                            throwable?.let {

                                launch(UI){
                                    alert {
                                        title = "Error"
                                        message = "Invalid keys. Press OK to reconfigure terminal"
                                        isCancelable = false
                                        okButton { configureTerminal()
                                        }
                                    }.show()

                                }
                                return@let
                            }

                            configData?.let {
                                doAsync {
                                    db.configDataDao.save(it)
                                    Log.d("OkH", it.data.toString())
                                    SharedPreferenceUtils.setIsTerminalPrepped(this@TermMagmActivity, true)
                                }
                                launch(UI){
                                    progressDialog.dismiss()
                                    alert {
                                        title = "Success"
                                        message = "Device configured successfully"
                                        okButton { }
                                    }.show()
                                }

                            }
                        }
            }
        }
    }

    private fun getVasTerminalService() : Single<VasTerminalData>{
        return VasTerminalService.Factory.getService().getVasTerminalDetails()
    }



    private fun configureTerminal() {
        progressDialog.setMessage("Getting keys")
        progressDialog.show()


        Log.d("connectionData.terminalID  >>>>", connectionData.terminalID)

        Log.d("connectionData.terminalID Check  >>>>", connectionData.terminalID .equals(resources.getString(R.string.def_terminal_id)).toString())

        Log.d("connectionData.terminalID value  >>>>", resources.getString(R.string.def_terminal_id))


//        equals("ITEX")
        if (connectionData.terminalID.equals(resources.getString(R.string.def_terminal_id))){
            alert{
                title="Terminal ID not set"
                message="Click Ok to move to settings page"
                isCancelable=false
                okButton {
                    startActivity(Intent(this@TermMagmActivity,SettingsActivity::class.java))
                }
            }.show()

            progressDialog.dismiss()
            return
        }


        getVasTerminalService()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe {
                    terminalData, error ->

                    terminalData?.let {
                        launch {
                            (application as App).db.vasTerminalDataDao.save(it)
                        }

                        Log.d("ConnectionData  >>>>", Gson().toJson(connectionData))

                        val liveKeyHolder = hostInteractor.getKeyHolder(connectionData)
                        liveKeyHolder.subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe {keyHolder: KeyHolder?, throwable: Throwable? ->
                                    keyHolder?.let {
                                        progressDialog.setMessage("Configuring terminal")
                                        Log.d("keyHolder",keyHolder.toString())

                                        val isTest = when(sharedPreferences.getString(getString(R.string.key_platform), "").toLowerCase()){
                                            "test" -> true
                                            else -> false
                                        }

                                        if (isTest)
                                            it.isTestPlatform = true

                                        async {
                                            db.keyHolderDao.save(it)
                                        }
                                        Log.d("connectionData",connectionData.toString())
                                        hostInteractor.getConfigData(connectionData, it)
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribeOn(Schedulers.io())
                                                .subscribe { configData , throwable ->
                                                    configData?.let {

                                                        doAsync {
                                                            db.configDataDao.save(configData)
                                                            SharedPreferenceUtils.setIsTerminalPrepped(this@TermMagmActivity, true)
                                                        }

                                                        progressDialog.dismiss()
                                                        alert{
                                                            title = "Success"
                                                            message = "Device configured successfully"
                                                        }.show()
                                                    }

                                                    throwable?.let {
                                                        progressDialog.dismiss()
                                                        displayError(it as Exception)
                                                    }
                                                }
                                    }


                                    throwable?.let {
                                        progressDialog.dismiss()
                                        toast("Error")
                                        displayError(it as Exception)
                                    }
                                }

                    }

                    error?.let {
                        progressDialog.dismiss()
                        alert {
                            title = "Error"
                            message = it.message.toString()
                            okButton {  }
                        }.show()
                    }
                }
    }
}