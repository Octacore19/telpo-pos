package com.iisysgroup.androidlite.payments_menu.handlers

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.LiveDataReactiveStreams
import android.arch.lifecycle.Transformations
import android.util.Log
import com.iisysgroup.poslib.commons.emv.EmvTransactionType
import com.iisysgroup.poslib.deviceinterface.interactors.EmvInteractor
import com.iisysgroup.poslib.host.Host
import com.iisysgroup.poslib.host.HostInteractor
import com.iisysgroup.poslib.host.dao.PosLibDatabase
import com.iisysgroup.poslib.host.entities.ConfigData
import com.iisysgroup.poslib.host.entities.ConnectionData
import com.iisysgroup.poslib.host.entities.KeyHolder
import com.iisysgroup.poslib.host.entities.TransactionResult
import com.iisysgroup.poslib.utils.InputData
import com.iisysgroup.poslib.utils.TransactionData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.runBlocking

class VasPurchase(owner: LifecycleOwner, db: PosLibDatabase, val inputData: InputData,
                  val hostInteractor: HostInteractor, val connData: ConnectionData,
                  val emvInteractor: EmvInteractor, val configData: ConfigData, val keyHolder: KeyHolder)  {


    init {
        emvInteractor.observeStatus().subscribe {
            Log.d("VAS_PURCHASE", it.state.toString())
        }
    }


    fun getTransactionResult(): Single<TransactionResult> {
        val cardData = emvInteractor.startEmvTransaction(inputData.amount,
                inputData.additionalAmount, EmvTransactionType.EMV_PURCHASE).subscribeOn(Schedulers.io())


        return cardData.flatMap {

            val transactionData = TransactionData(inputData, it, configData, keyHolder)
            hostInteractor.getTransactionResult(Host.TransactionType.PURCHASE, connData,
                    transactionData, null, null).subscribeOn(Schedulers.io())
        }
    }


}


