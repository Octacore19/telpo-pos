package com.iisysgroup.androidlite;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.iisysgroup.androidlite.vas.activity.AllVasActivity;
import com.iisysgroup.androidlite.vas.activity.CableTVActivity;
import com.iisysgroup.androidlite.vas.activity.Genesis.MovieHouse;
import com.iisysgroup.androidlite.vas.activity.education.EduActivity;
import com.iisysgroup.androidlite.vas.activity.EnergyActivity;
import com.iisysgroup.androidlite.vas.activity.EventsActivity;
import com.iisysgroup.androidlite.vas.activity.GamesActivity;
import com.iisysgroup.androidlite.vas.activity.Insurance;
import com.iisysgroup.androidlite.vas.internet.InternetSubscription;
import com.iisysgroup.androidlite.vas.airtime_and_data.SelectionActivity;

public class VasActivity extends AppCompatActivity implements View.OnClickListener{
    Toolbar toolbar;
    TextView all, energy,cableTV,airtime,internet_subsc,edu_btn,games,events,insurance, tickets;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home : onBackPressed();
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vas);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        all = findViewById(R.id.all);
        internet_subsc = findViewById(R.id.internet_subscription);
        energy = findViewById(R.id.energy);
        cableTV = findViewById(R.id.cableTV);
        airtime = findViewById(R.id.airtimeAndData);
        edu_btn = findViewById(R.id.edu);
        games = findViewById(R.id.games);
        events = findViewById(R.id.events);
        insurance = findViewById(R.id.insurance);
        tickets = findViewById(R.id.tikets);

        all.setOnClickListener(this);
        energy.setOnClickListener(this);
        cableTV.setOnClickListener(this);
        airtime.setOnClickListener(this);
        internet_subsc.setOnClickListener(this);
        edu_btn.setOnClickListener(this);
        games.setOnClickListener(this);
        events.setOnClickListener(this);
        insurance.setOnClickListener(this);
        tickets.setOnClickListener(this);
        //money_transfer = findViewById(R.id.money_transfer);money_transfer.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.all:
                intent = new Intent(this, AllVasActivity.class);
                startActivity(intent);
                break;
            case R.id.energy:
                intent = new Intent(this, EnergyActivity.class);
                startActivity(intent);
                break;
            case R.id.cableTV:
                intent = new Intent(this, CableTVActivity.class);
                startActivity(intent);
                break;
            case R.id.airtimeAndData:
                //Show user activity for selection of either data or airtime

                intent = new Intent(this, SelectionActivity.class);
                startActivity(intent);
                break;
            case R.id.internet_subscription:
                intent = new Intent(this, InternetSubscription.class);
                startActivity(intent);
                break;
            case R.id.edu:
                intent = new Intent(this, EduActivity.class);
                startActivity(intent);
                break;
            case R.id.games:
                intent = new Intent(this, GamesActivity.class);
                startActivity(intent);
                break;
            case R.id.events:
                intent = new Intent(this,EventsActivity.class);
                startActivity(intent);
                break;
            case R.id.insurance:
                intent = new Intent(this,Insurance.class);
                startActivity(intent);
                break;
            case R.id.tikets:
                intent = new Intent(this, MovieHouse.class);
                startActivity(intent);
                break;
            /*case R.id.money_transfer:
                intent = new Intent(this,MoneyTransferActivity.class);
                startActivity(intent);
                break;*/
        }
    }
}
