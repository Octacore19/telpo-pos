package com.iisysgroup.androidlite.pfm;

import com.google.gson.Gson;

public class NotificationModel {
    private PfmJournal pfmJournal;
    private  PfmState pfmState;
    public NotificationModel(PfmState state, PfmJournal journal){
        this.pfmJournal = journal;
        this.pfmState = state;
    }

    public String build(String notificationReceiver){
        Gson gson = new Gson();
        String stateString = gson.toJson(this.pfmState, PfmState.class);
        String journalString = gson.toJson(this.pfmJournal, PfmJournal.class);
        return "{ \"state\": "+stateString+", \"journal\": "+journalString+", \"getRRN\": \"true\", \"requestType\" : \""+notificationReceiver+"\" }";
    }

}
