package com.iisysgroup.androidlite.history_summary.model

data class BalanceModel(var status : Int, var error : Boolean, var message : String, var date : String, var errors : String,
                        var balance : Float, var commissionBalance : Float, var  walletID : String, var name : String, var email : String)